<?php
class Database
{
    private $conn = null;
    private $table = '';
    private $sql = '';

    /**
     * @param string $table
     */

    public function setTable($table)
    {
        $this->table = $table;
    }

    public function __construct($table)
    {
        $this->connect();
        $this->setTable($table);
    }

    public function insert($data)
    {
        $fieldsArray = [];
        $valuesArray = [];
        foreach ($data as $field => $value){
            $fieldsArray[] = "`$field`";
            $valuesArray[] = "'$value'";
        }

        $fieldsStr = implode(',',$fieldsArray);
        $valuesStr = implode(',',$valuesArray);


        $this->sql = "INSERT INTO `$this->table` ($fieldsStr) VALUES ($valuesStr)";
        return $this;
    }

    public function save(){
        return $this->conn->query($this->sql);
    }

    public function select($what = "*")
    {
        $whatStr = '*';
        if(is_array($what)){
            $whatArr = [];
            foreach ($what as $value){
                $whatArr[] = "`$value`";
            }
            $whatStr = implode(',',$whatArr);
        }else{
            if($what != "*"){
                $whatStr = "`$what`";
            }
        }

        $this->sql = "SELECT $whatStr FROM `$this->table`";

        return $this;
    }

    public function update($data1)
    {    $condArr1 = [];
        foreach ($data1 as $field => $value){
            $condArr1[] = "$field='$value'";
        }
        $condStr1 = implode(' , ',$condArr1);

        $this->sql = "UPDATE $this->table SET $condStr1";
        return $this;

    }

    public function delete()
    {  $this->sql = "DELETE FROM $this->table";
        return $this;
    }


    public function where($conditions = 1)
    {
        $condStr = 1;
        if(is_array($conditions)){
            $condArr = [];
            foreach ($conditions as $field => $value){
                if(strpos($value,',')){
                    $condArr[] = "`$field` in ($value)";
                } else{
                    $condArr[] = "`$field`='$value'";
                }

            }
            $condStr = implode(' AND ',$condArr);
        }

        $this->sql .= " WHERE $condStr";
        return $this;
    }

    public function one(){
        return $this->save()?$this->save()->fetch_assoc():false;
    }

    public function all(){
        return $this->save()?$this->save()->fetch_all(MYSQLI_ASSOC):false;
    }

    private function connect(){
        $confs = parse_ini_file('configs/db.ini');
        $this->conn = new Mysqli($confs['host'],$confs['username'],$confs['password'],$confs['dbname']);
    }
}