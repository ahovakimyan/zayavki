<?php
include_once "helpers/Database.php";
$id = $_POST['id'];

if($id){
    $db = new Database("applications");
    $app = $db->select()->where(["application_id"=>$id])->one();

    if(!$app){
        if($db->insert(['application_id'=>$id])->save()){
            echo json_encode([
                'success'=>true
            ]);die;
        }
    }else{
        echo json_encode([
            'success'=>true
        ]);die;
    }
}

echo json_encode([
    'success'=>false
]);die;

