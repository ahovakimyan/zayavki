<?php
    include_once "../helpers/applications.php";
    include_once "layouts/header.php";
?>

        <div class="row">
            <h1 align="center">Заявки</h1>
        </div>
        <?php
            if(isset($_SESSION['create_success'])){
             if($_SESSION['create_success']){
        ?>
                 <div class="alert alert-success" role="alert">
                      Created Successfully
                 </div>
        <?php
             } else {
         ?>
                 <div class="alert alert-danger" role="alert">
                     Creation Error
                 </div>
         <?php
             }
            }
        ?>
        <?php foreach ($notifications as $id => $notification) {
            if($notification['success']){?>
                <div class="alert alert-primary" role="alert">
                    Changed status of application #<?php echo $id ?> to "sent"
                </div>
            <?php }else{ ?>
                <div class="alert alert-danger" role="alert">
                    There was error when trying to change status of application #<?php echo $id ?> to "sent"
                </div>
            <?php } ?>
        <?php } ?>
        <div class="row">
            <table class="table">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Name</th>
                        <th>Status</th>
                        <th>Amount</th>
                        <th>Date</th>
                        <th>Created</th>
                        <th>Updated</th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($applications as $application) { ?>
                        <tr>
                            <td><code>#<?php echo $application['id'] ?></code></td>
                            <td><?php echo $application['name'] ?></td>
                            <td><?php echo $application['status'] ?></td>
                            <td><?php echo $application['amount'] ?>$</td>
                            <td><?php echo $application['date'] ?></td>
                            <td><?php echo $application['created_at'] ?></td>
                            <td><?php echo $application['updated_at'] ?></td>
                        </tr>
                    <?php } ?>
                </tbody>
            </table>
        </div>
        <div class="row">
            <a href="send.php" class="btn btn-success">
                Send
            </a>
            <a href="new" class="btn btn-primary ml-3">
                New
            </a>
        </div>
<?php
    include_once "layouts/footer.php";
    session_destroy()
?>