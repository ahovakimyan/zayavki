<?php
    session_start();
    include_once "layouts/header.php";
?>
    <h1 class="mb-5">Create New</h1>
        <?php
            if(isset($_SESSION['error'])){?>
                <ul class="alert alert-danger" role="alert">
                    <?php
                        foreach($_SESSION['error'] as $key => $err){
                    ?>
                        <li><?php echo $err ?></li>
                    <?php
                        }
                    ?>
                </ul>
            <?php } ?>
    <form action="/create-new" method="post">
        <div class="row">
            <div class="col">
                <div class="form-group">
                    <label for="name">Name</label>
                    <input name="name" type="text" class="form-control" id="name" aria-describedby="name" placeholder="Name">
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col">
                <div class="form-group">
                    <label for="amount">Amount</label>
                    <div class="input-group mb-3">
                        <input name="amount" type="number" class="form-control" id="amount" aria-describedby="amount" placeholder="Amount">
                        <div class="input-group-prepend">
                            <span class="input-group-text">$</span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col">
                <div class="form-group">
                    <label for="date">Date</label>
                    <input name="date" type="date" class="form-control" id="date" aria-describedby="date" placeholder="Date">
                </div>
            </div>
        </div>
        <button type="submit" class="btn btn-primary">Submit</button>
    </form>
<?php
    session_destroy();
    include_once "layouts/footer.php";
?>