<?php
session_start();
include_once "../helpers/Database.php";
$post = $_POST;

//var_dump($post);die;

if(!isset($post['name']) || empty($post['name'])){
    $_SESSION['error']['name'] = "Please write name";
}

if(!isset($post['amount']) || empty($post['amount'])){
    $_SESSION['error']['amount'] = "Please write amount";
}
if(!isset($post['date']) || empty($post['date'])){
    $_SESSION['error']['date'] = "Please write date";
}

if(isset($_SESSION['error'])){

    header('location:/new');die;
}

$db = new Database("applications");

if($db->insert([
    'name'=>$post['name'],
    'amount'=>$post['amount'],
    'date'=>$post['date'],
])->save()){
    $_SESSION['create_success'] = true;
}else{
    $_SESSION['create_success'] = false;
};

header('location:/');die;