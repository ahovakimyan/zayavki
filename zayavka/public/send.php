<?php
session_start();
include_once "../helpers/Database.php";
$db = new Database("applications");

$applications = $db->select()->where(['status'=>"'new','error'"])->all();
$_SESSION['application_send_request'] = [];
$bank_api = parse_ini_file("../configs/bank_api.ini")['bank_api_url'];

foreach ($applications as $application){
    $data = array(
        'id' => $application['id']
    );

    $url = $bank_api.'/send';
    $ch = curl_init($url);

    $postString = http_build_query($data, '', '&');

    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $postString);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

    $response = json_decode(curl_exec($ch));
    curl_close($ch);

    if($response->success){
        $db->update(['status'=>'sent','updated_at'=>date("Y-m-d H:m:s",time())])->where(['id'=>$application['id']])->save();
        $_SESSION['application_send_request'][$application['id']] = ['success'=>true];
    }else{
        $db->update(['status'=>'error','updated_at'=>date("Y-m-d H:m:s",time())])->where(['id'=>$application['id']])->save();
        $_SESSION['application_send_request'][$application['id']] = ['success'=>false];
    }
}

header('location:/');die;
